// Do NOT use .includes, to complete this function.

function find(elements, cb) {
    let ans;
    if (!Array.isArray(elements) || elements.length === 0 || typeof cb !== 'function')  // Data checks ( To work only with arrays.)
        return [];

    for (let i = 0; i < elements.length; i++)                  // Look through each value in `elements` and pass each element to `cb`.
    {
        if (cb(elements[i], i, elements))                             // If `cb` returns `true` then return that element.
            return elements[i];
    }

    return ans;                                        // Return `undefined` if no elements pass the truth test.                         
}

module.exports = find;