 // Do NOT use .map, to complete this function.
// How map works: Map calls a provided callback function once for each element in an array, in order, and function constructs a new array from the res .
// Produces a new array of values by mapping each value in list through a transformation function (iteratee).
// Return the new array.

function map(elements, cb) {
    let ans = [];
        if(!Array.isArray(elements) || elements.length === 0 || typeof cb !== 'function' )  // Data checks ( To work only with arrays.)
            return ans;
    if(cb.length > 1)                                                                       //Checks number  of arguments to be passed to the function 
        {
            for(let i=0;i<elements.length;i++)                                                 
                ans.push(cb(elements[i],i,elements));                                                         //Pass the index into `cb` as the second argument
        }                                                             
    else 
        {
            for(let i=0;i<elements.length;i++)
                ans.push(cb(elements[i]));
        }
    return ans;                                                                             
}

module.exports = map;