function each(elements, cb) {
    let ans;
    if (!Array.isArray(elements) || elements.length === 0 || typeof cb !== 'function')  // Data checks ( To work only with arrays.)
        return [];
    if (cb.length > 1)                                                                       //Checks number  of arguments to be passed to the function 
    {
        for (let i = 0; i < elements.length; i++)
            cb(elements[i], i,elements);                                                         //Pass the index into `cb` as the second argument
    }
    else {
        for (let i = 0; i < elements.length; i++)
            cb(elements[i]);
    }
    return ans;                                                                             //Returns undefined as returned by forEach function
}

module.exports = each;