function flat(elements){
let ans = [];

for(let i=0;i<elements.length;i++){
    if(!Array.isArray(elements[i]))
        ans.push(elements[i]);
    else 
        ans = ans.concat(flat(elements[i]));
}
return ans;
}

module.exports = flat;