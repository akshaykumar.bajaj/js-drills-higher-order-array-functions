// Do NOT use .reduce to complete this function.
// How reduce works: A reduce function combines all elements into a single value going from left to right.

function reduce(elements, cb, startingValue) {
    let ans = [], i;
    if (!Array.isArray(elements) || elements.length === 0 || typeof cb !== 'function')  // Data checks ( To work only with arrays.)
        return ans;

    if (startingValue == null) {
        ans = elements[0],                  //If `startingValue` is undefined then make `elements[0]` the initial value.
            i = 1;
    }
    else {
        ans = startingValue;
        i = 0;
    }

    for (i; i < elements.length; i++) {    // Elements will be passed one by one into `cb` along with the `startingValue`.
        ans = cb(ans, elements[i],i,elements);     // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    }

    return ans;
}

module.exports = reduce;