let each = require('../each.js');

const items = [1, 2, 3, 4, 5, 5];

const result = each(items, console.log);

if (result) console.log(result);
