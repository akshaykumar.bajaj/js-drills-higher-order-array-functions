let filter = require('../filter.js');

const items = [1, 2, 3, 4, 5, 5];

//Test Eaxmple 1

const result = filter(items, element => element > 3);
console.log(result);

//Test Eaxmple 2: filter prime numbers in an array

function isPrime(element, index, array) {
  let start = 2;
  while (start <= Math.sqrt(element)) {
    if (element % start++ < 1) {
      return false;
    }
  }
  return element > 1;
}

console.log(filter([4, 6, 8, 12], isPrime)); //No prime numbers found , returns emptfunc array
console.log(filter(items, isPrime));         // 2 3 5 5 

