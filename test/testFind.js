let find = require('../find.js');

const items = [1, 2, 3, 4, 5, 5];

//Test Eaxmple 1

const result = find(items, element => element > 3);
console.log(result);

//Test Eaxmple 2: Find a prime number in an array

function isPrime(element, index, array) {
  let start = 2;
  while (start <= Math.sqrt(element)) {
    if (element % start++ < 1) {
      return false;
    }
  }
  return element > 1;
}

console.log(find([4, 6, 8, 12], isPrime)); // undefined, not found
console.log(find([4, 5, 8, 12], isPrime)); // 5

