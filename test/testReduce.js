let reduce = require('../reduce.js');

const items = [1, 2, 3, 4, 5, 5];

//Test Exmaple 1: Sum of all elements in array
const result = reduce(items, function (accumulator, currentValue) {
  return accumulator + currentValue
}, 0);

console.log(result);

//Test Example 2: Removing duplicates from array

let myArrayWithNoDuplicates = reduce(items, function (accumulator, currentValue) {
  if (accumulator.indexOf(currentValue) === -1) {
    accumulator.push(currentValue)
  }
  return accumulator
}, []);

console.log(myArrayWithNoDuplicates);