// Do NOT use .filter, to complete this function.
// Similar to `filter` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test
function filter(elements, cb) {
    let ans = [];
    if (!Array.isArray(elements) || elements.length === 0 || typeof cb !== 'function')  // Data checks ( To work only with arrays.)
        return ans;

    for (let i = 0; i < elements.length; i++) {
        if (cb(elements[i], i, elements))
            ans.push(elements[i]);
    }

    return ans;
}

module.exports = filter;